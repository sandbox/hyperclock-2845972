# vtxDownloads



### What is vtxDownloads ?


vtxDownloads is an advanced Downloads module for your Drupal 8.x site.
With this powerful module, you can easily set a downloads section on your
site and offer unlimited amount of files to your users.


### Features

  * supports subcategories
  * integrated permissions system
  * a complete notifications system


### How to install or Update vtxDownloads


For information on how to install or update this module, please read the module documentation, available in the module package:

\vtxdownloads\docs\english\readme.install.txt

!!!  Please read the docs before updating or installing !!!
