<?php

/**
* @file
* Contains \Drupal\vtxdownloads\Controller\vtxDownloadsController.
*/

namespace Drupal\vtxdownloads\Controller;

use Drupal\Core\Controller\ControllerBase;

class vtxDownloadsController extends ControllerBase {
    public function content() {
        return array(
            '#type' => 'markup',
            '#markup' => t('<br> With this powerful module, you can easily set a downloads section on your site and offer unlimited amount of files to your users.'),
        );
    }
}
